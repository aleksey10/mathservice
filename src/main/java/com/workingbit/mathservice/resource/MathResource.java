package com.workingbit.mathservice.resource;

import com.workingbit.mathservice.common.ResourceConstants;
import com.workingbit.mathservice.service.MathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Aleksey Popryaduhin on 09:44 29/08/2017.
 */
@RestController
@RequestMapping(ResourceConstants.MATH_SERVICE)
public class MathResource {

  private final MathService mathService;

  @Autowired
  public MathResource(MathService mathService) {
    this.mathService = mathService;
  }

  @RequestMapping(ResourceConstants.COS_SEQUENCE_SERVICE)
  public String cosSequence(@RequestParam String n) {
    return mathService.getCosSequence(n);
  }
}
