package com.workingbit.mathservice.common;

/**
 * Created by Aleksey Popryaduhin on 09:45 29/08/2017.
 */
public class ResourceConstants {
  public static final String MATH_SERVICE = "/math";
  public static final String COS_SEQUENCE_SERVICE = "/cos/sequence";
}
