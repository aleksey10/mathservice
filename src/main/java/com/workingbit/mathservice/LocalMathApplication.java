package com.workingbit.mathservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Aleksey Popryaduhin on 09:42 29/08/2017.
 */
@SpringBootApplication
public class LocalMathApplication {

  public static void main(String[] args) {
    SpringApplication.run(LocalMathApplication.class);
  }
}
