package com.workingbit.mathservice.service;

import org.springframework.stereotype.Service;

/**
 * Created by Aleksey Popryaduhin on 09:50 29/08/2017.
 */
@Service
public class MathService {
  public String getCosSequence(String n) {
    if (n == null || n.isEmpty()) {
      return "Sn = undefined";
    }
    return "Sn = " + getSn(Integer.parseInt(n));
  }

  String getSn(int n) {
    return getSn(1, n);
  }

  private String getSn(int i, int n) {
    if (n == 1) {
      return getAn(1) + " + " + i;
    }
    return "(" + getSn(i + 1, n - 1) + ") * " + getAn(n) + " + " + i;
  }

  String getAn(int n) {
    return getAn(1, n);
  }

  private String getAn(int i, int n) {
    String s = i % 2 == 0 ? " + " : " - ";
    if (n == i) {
      return "cos(" + n + ")";
    }
    return "cos(" + i + s + getAn(i + 1, n) + ")";
  }
}
