package com.workingbit.mathservice.service;

import org.junit.Test;

import static com.workingbit.mathservice.common.AppConstants.SCALE;
import static org.junit.Assert.assertEquals;

/**
 * Created by Aleksey Popryaduhin on 09:51 29/08/2017.
 */
public class MathServiceTest {

  private MathService mathService = new MathService();
  private double delta = 0.0000001;

  @Test
  public void getCosSequence() throws Exception {
    String cosSequence = mathService.getCosSequence("3");
    assertEquals("Sn = ((cos(1) + 3) * cos(1 - cos(2)) + 2) * cos(1 - cos(2 + cos(3))) + 1", cosSequence);
  }

  @Test
  public void get_A2() {
    String a2 = mathService.getAn(2);
    assertEquals("cos(1 - cos(2))", a2);
  }

  @Test
  public void get_A3() {
    String a3 = mathService.getAn(3);
    assertEquals("cos(1 - cos(2 + cos(3)))", a3);
  }

  @Test
  public void get_S3() {
    String s3 = mathService.getSn(3);
    assertEquals("((cos(1) + 3) * cos(1 - cos(2)) + 2) * cos(1 - cos(2 + cos(3))) + 1", s3);
  }

  @Test
  public void get_S1() {
    String s3 = mathService.getSn(1);
    assertEquals("cos(1) + 1", s3);
  }

  private double getWithScale(double value) {
    return Math.round(value * SCALE) / SCALE;
  }

}